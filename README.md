**_Environments and deployments_**

The repository contains .Net application and infrastructure code.

**_Architecture_**

For the project created CI/CD Pipeline based on Gitlab-CI, Terraform and K8s. The pipeline has six stages:

<table>
<tr>
<th>stage</th>
<th>tool</th>
<th>description</th>
</tr>
<tr>
<td>build</td>
<td>.NET 6.0</td>
<td>The stage builds th application.</td>
</tr>
<tr>
<td>test</td>
<td>.NET 6.0</td>
<td>The stage has 3 test types. Unit tests, Integration tests and Functionality tests.</td>
</tr>
<tr>
<td>scan</td>
<td>SonarQube</td>
<td>The stage does SonarQube scan.</td>
</tr>
<tr>
<td>ship</td>
<td>

Docker

Alpine
</td>
<td>The stage does docker ship for two images to GitLab Container Registry. The stage pushes new Helm chart to S3.</td>
</tr>
<tr>
<td>eksdeploy</td>
<td>Terraform</td>
<td>The stage creates AWS EKS environment for deployment. The stage destroys environment(manual).</td>
</tr>
<tr>
<td>gitops</td>
<td>ArgoCD</td>
<td>The stage creates integrates gitops practices to the project.</td>
</tr>
</table>

**_Quickstart_**

Step 1.

* Create two S3 buckets for Terraform state file with -dev and -prod endings.
* Create two DynamoDB tables with -dev and -prod endings.
* Open .gitlab-ci.yml file. In "AWS EKS Deploy" job change "-backend-config" configs to your DynamoDB table and S3 bucket.
* Create Gitlab-runner with Docker executor and "eshop" tag.

Step 2: Define all necessary variables in GitLab main project.

<table>
<tr>
<th>Type</th>
<th>Key</th>
<th>Description</th>
</tr>
<tr>
<td>Variable</td>
<td>

<div>

<div>

<span dir="">AWS_ACCESS_KEY_ID</span>

</div>
</div></td>
<td>Your access key id.</td>
</tr>
<tr>
<td>Variable</td>
<td>AWS_SECRET_ACCESS_KEY</td>
<td>Your secret access key.</td>
</tr>
<tr>
<td>Variable</td>
<td>AWS_DEFAULT_REGION</td>
<td>Your preferred AWS region</td>
</tr>
<tr>
<td>Variable</td>
<td>SONAR_HOST_URL</td>
<td>SonarQube URL.</td>
</tr>
<tr>
<td>Variable</td>
<td>SONAR_TOKEN</td>
<td>SonarQube token.</td>
</tr>
<tr>
<td>Variable</td>
<td>

<div>

<div>

<span dir="">SONAR_PROJECT_NAME</span>

</div>
</div></td>
<td>Project name.</td>
</tr>
<tr>
<td>Variable</td>
<td>SONAR_USER</td>
<td>SonarQube scanner username.</td>
</tr>
<tr>
<td>Variable</td>
<td>

<div>

<div>

<span dir="">ZONE_ID</span>

</div>
</div></td>
<td>AWS Route53 Zone ID.</td>
</tr>
</table>

Step 3: Create S3 bucket for Helm charts with "eshop_helm" name.

* Create new S3 bucket for Helm charts with "eshop_helm" name.
* Pull Helm chart. <https://gitlab.com/henrikamirbekyan/eshop_helm>
* Change domains in ingress.yaml file with preferred.
* Package chart with 0.0.0 tag.
* Push it to S3.
* In project create the new tag you have specified and push it. It will trigger Gitlab CI.
* In case of production deploying it has to be a tag with -stable ending (example: 5.0.1-stable).

Step 4: Create S3 bucket for ArgoCD application deployment file with "eshop-prod-application" name.

* When we push a tag with -stable ending it will create an application.yaml file in that bucket with that tag for production deployment. Only stable images are meant to be in production.
* Change domains in argoCD/batch.j2 with preferred (you changed it in ingress.yaml file).
* Let Terraform, K8s and argoCD do the magic.

# Terraform

## Requirements
| Name | Version |
|------|---------|
| [terraform](#requirement_terraform) | >= 1.1 |

## Providers
| Name | Version |
|------|---------|
| [aws](#provider_aws) | n/a |
| [http](#provider_http) | n/a |

## Modules

No modules.

## Resources
| Name | Type |
|------|------|
| [aws_eks_cluster.cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster) | resource |
| [aws_eks_node_group.nodegroup](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_node_group) | resource |
| [aws_iam_role.cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.node](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.cluster-AmazonEKSClusterPolicy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.cluster-AmazonEKSVPCResourceController](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.node-AmazonEC2ContainerRegistryReadOnly](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.node-AmazonEKSWorkerNodePolicy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.node-AmazonEKS_CNI_Policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_internet_gateway.igw](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | resource |
| [aws_route_table.rt](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table_association.rta](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_security_group.cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.cluster-ingress-workstation-https](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_subnet.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_vpc.vpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |
| [http_http.workstation-external-ip](https://registry.terraform.io/providers/hashicorp/http/latest/docs/data-sources/http) | data source |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|----------|
| [aws_region](#input_aws_region) | n/a | `string` | `"eu-central-1"` | no |
| [env](#input_env) | n/a | `string` | n/a | yes |

## Outputs
| Name | Description |
|------|-------------|
| [config_map_aws_auth](#output_config_map_aws_auth) | n/a |
| [kubeconfig](#output_kubeconfig) | n/a |
